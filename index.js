const express = require('express');
const app = express();
const UserS = require("./models/Users");
const  bcrypt = require("bcrypt");
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Users = require('./models/Users');
mongoose.set('debug', true);
const mongoDB = process.env.MONGODB_URI 
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.on('connecting',console.debug.bind(console, 'MongoDB connecting Debug'));
db.on('connected',console.debug.bind(console, 'MongoDB connected Debug'));
const port = process.env.PORT || 8080;

const saltRounds = 10;
const jsonParser = bodyParser.json();
app.post("/register",jsonParser, async (req, res)=>{
    console.log(req.body);
    try {
      const hashedPwd = await bcrypt.hash(req.body.password, saltRounds);
      const userDto = await UserS.create({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPwd,
        bio: req.body.bio
      });
      res.send(userDto);
    } catch (error) {
      console.log(error);
      res.status(500).send("Server is failed");
    }
  });

app.post("/login", async (req, res) => {
    try {
      const {userDto, hashedPwd} = req.body;
      const user = await UserS.findOne({email: req.body.email});
      if (!user) {
        return res.status(400)
            .send({message: 'Invalid email'});
      }
      const checkPassword = await bcrypt.compare(hashedPwd, userDto.password);

      if (!checkPassword) {
        return res.status(400)
            .send({message: 'Invalid password'});
      }
      const token = jwt.sign({
        _id: user._id,
        username: user.email,
      }, process.env.SECRET_KEY);

      res.status(200).send({message: 'Success', jwt_token: token});
    } catch (e) {
      res.status(400).send({message: e.message});
    }
  });

    
    



app.listen(port);//app started
