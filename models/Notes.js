const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const NoteSchema = new Schema(
    {
      author: {type: Schema.ObjectId, ref: 'User', required: true},
      title: {type: String},
      date: {type: Date},
      info: {type: String},
  });

module.exports = mongoose.model('Note', NoteSchema);
  